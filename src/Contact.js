import React, { useState, useEffect } from 'react';
import { Avatar } from '@material-ui/core';
import './Contact.css';

function Contact() {

    const [contacts, setContacts] = useState([]);

    useEffect(() => {
        const getContactsData = async () => {
            await fetch("https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json")
            .then((response) => response.json())
            .then((data) => {
                const contacts = data.map((contact) => (
                    {
                        id: contact.id,
                        firstName: contact.first_name,
                        lastName: contact.last_name,
                        avatar: contact.avatar,
                    }
                ))
                contacts.sort(function (a, b) {
                    if (a.lastName < b.lastName) {return -1;}
                    if (a.lastName > b.lastName) {return 1;}
                    return 0;
                })
                setContacts(contacts);
            })
        }

        getContactsData();
    }, [])

    // console.log(contacts);

    return (
        <div className="contact">
            <div className="contact__input">
                <input 
                    className="contact__search" 
                    type="text" 
                    placeholder="Search..."
                />
            </div>
            {
                contacts.map((contact) => (
                    <div className="contact__info">
                            <Avatar src={contact.avatar} className="contact__avatar"/>
                            <p className="contact__name">{contact.firstName} {contact.lastName}</p>
                            <input 
                                type="checkbox"
                                className="contact__checkbox"
                            />
                    </div>
                ))
            }
        </div>
    )
}

export default Contact;
