import React from 'react';
import './App.css';
import Contact from './Contact';
import Nav from './Nav';

function App() {
  return (
    <div className="app">
      <div className="app__header">
        <Nav />
      </div>
      <div className="app__contact" >
        <Contact />
      </div>
    </div>
  );
}

export default App;